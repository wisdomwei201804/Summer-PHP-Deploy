<?php
/**
 * 适用于命令行触发的部署
 */

//读取命令行参数
$arrArg = getopt('p:v::a::');
if (empty($arrArg['p'])) {
    echo '-p参数值不存在'.PHP_EOL."用法举例: php ./index_cli.php -p script_name".PHP_EOL;
    // php ./index_cli.php -p script_name -a1 全部复制
    exit;
}

//获取需要部署的项目名称, 也是项目的目录名
$strProjName = $arrArg['p'];
$isAll = isset($arrArg['a']) ? true : false;

//定义部署脚本路径

$deployDir = __DIR__.'/'; //默认部署脚本跟入口文件在同一目录
define('ROOT', str_replace('\\', '/', $deployDir));
$scriptFile = ROOT ."scripts/{$strProjName}.php"; //子部署脚本

if (file_exists($scriptFile) === FALSE) {
    echo "脚本: {$scriptFile} 不存在".PHP_EOL;
}

include_once('Tool.php');
    include($scriptFile);
    $obj = new $strProjName;
    $obj->deploy($isAll);


// 每5s执行一次
//for ($i = 0; $i < 10; $i++) {
//    //执行对应的部署脚本
//    echo date('Y-m-d H:i:s').PHP_EOL;
//    include("/usr/local/bin/deploy/{$strProjName}.php");
//    sleep(5);
//}
